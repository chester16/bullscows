<?php
/**
 * Created by PhpStorm.
 * User: rasskazov
 * Date: 10/13/18
 * Time: 6:56 PM
 */
namespace App\libs;

class TargetNumber
{
    static function createNumber(){
        $numbers = range(0,9);
        while ($numbers[0] == 0){
            shuffle($numbers);
        }
        $numbers = array_slice($numbers,0,4);
        return implode($numbers);
    }

    static function compareNumbers($number_base,$input_number){
        $result = [];

        foreach (str_split($input_number) as $key=>$value) {
            $pos = strpos($number_base,$value);

            if($pos >= 0 && $pos!==false){
                if($pos == $key){
                    array_push($result,1);
                }else{
                    array_push($result,0);
                }
            }
        }
        return $result;
    }
}