<?php

namespace App\Http\Controllers;

use App\libs\TargetNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;


class SiteController extends Controller
{
    public function index(){
        Redis::set('rgr',1234);
        $user = Redis::get('rgr');
        var_dump($user);
        return view('welcome');
    }

    public function newNumber(Request $request){
        $token = $request->post('token');
        $number = TargetNumber::createNumber();
        Redis::set($token,$number);
        return TargetNumber::createNumber();
    }

    public function checkNumber(Request $request){
        $token = $request->post('token');
        $number = $request->post('number');
        $result = TargetNumber::compareNumbers(Redis::get($token),$number);
        if(is_numeric($number) && strlen($number)==4){

            return json_encode(['status'=>true,'number'=>$number,'result'=>$result]);
        }
        return json_encode(['status'=>false,'number'=>$number]);
    }
}
